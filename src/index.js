import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Dashboard from './componentes/Dashboard';
import Peliculas from './componentes/Peliculas';
import Turnos from './componentes/Turnos';
import Administradores from './componentes/Administradores';
import Perfil from './componentes/Perfil';
import CerrarSesion from './componentes/CerrarSesion';
import CrearPelicula from './componentes/CrearPelicula';
import ActualizarPelicula from './componentes/ActualizarPelicula';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route exact path="/" component={Peliculas} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route exact path="/peliculas" component={Peliculas} />
      <Route exact path="/turnos" component={Turnos} />
      <Route exact path="/administradores" component={Administradores} />
      <Route exact path="/perfil" component={Perfil} />
      <Route exact path="/cerrar-sesion" component={CerrarSesion} />
      <Route exact path="/peliculas/create" component={CrearPelicula} />
      <Route exact path="/peliculas/:peliculaId" component={ActualizarPelicula} />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
