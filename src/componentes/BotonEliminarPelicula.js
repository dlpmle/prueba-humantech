import React from 'react';
import { FaTrashAlt } from 'react-icons/fa';

class BotonEliminarPelicula extends React.Component {
    render() {
        return (
            <button className="btn btn-light">
                <FaTrashAlt/>
            </button>
        );
    }

}

export default BotonEliminarPelicula;