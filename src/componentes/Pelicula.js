import React from 'react';
import BotonEditar from './BotonEditar';
import BotonAsignarTurno from './BotonAsignarTurno';
import BotonCambiarEstado from './BotonCambiarEstado';
import BotonEliminarPelicula from './BotonEliminarPelicula';
import { Link } from 'react-router-dom';

class Pelicula extends React.Component {

    editar = () => {
        console.log("Estoy en edición");
    }

    render() {
        return (<tr>
            <td scope="row">{this.props.id}</td>
            <td>{this.props.nombre}</td>
            <td>{this.props.fpublicacion}</td>
            <td>{this.props.estado == 1 ? "Activo" : "Inactivo"}</td>
            <td>
                <Link to={`/peliculas/${this.props.id}`}>
                    <BotonEditar onClick={this.editar} />
                </Link>
                <BotonAsignarTurno />
                <BotonCambiarEstado estado={this.props.estado} />
                <BotonEliminarPelicula />
            </td>
        </tr>);
    }
}

export default Pelicula;