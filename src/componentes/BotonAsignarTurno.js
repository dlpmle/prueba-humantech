import React from 'react';
import { FaGripLines } from 'react-icons/fa';

class BotonAsignarTurno extends React.Component {
    render() {
        return (
            <button className="btn btn-light">
                <FaGripLines />
            </button>
        );
    }

}

export default BotonAsignarTurno;