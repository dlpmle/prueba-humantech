import React from 'react';
import ListaPeliculas from './ListaPeliculas';
import { Link } from 'react-router-dom';

class Main extends React.Component {
    render() {
        return (
            <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div className="chartjs-size-monitor">
                    <div className="chartjs-size-monitor-expand">
                        <div className=""></div>
                    </div>
                    <div className="chartjs-size-monitor-shrink">
                        <div className=""></div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <h1>Películas</h1>
                        </div>
                        <div className="offset-md-4">
                            <button className="btn btn-success">
                                <Link to="/peliculas/create">Nueva Película</Link>
                            </button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                        <ListaPeliculas />
                        </div>  
                    </div>
                </div>
            </main>);
    }
}

export default Main;