import React from 'react';
import { FaLock } from 'react-icons/fa';
import { FaLockOpen } from 'react-icons/fa';

class BotonCambiarEstado extends React.Component {
    render() {
        return (
            <button className="btn btn-light">
                { this.renderTipoCandado() }
            </button>
        );
    }

    renderTipoCandado(){
        return this.props.estado == 1 ? <FaLock/> : <FaLockOpen/>;
    }

}

export default BotonCambiarEstado;