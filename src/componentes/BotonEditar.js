import React from 'react';
import { FaPencilAlt } from 'react-icons/fa';

class BotonEditar extends React.Component {
    
    editar = () => {
        this.props.onClick();
    }

    render() {
        return (
            <button className="btn btn-light" onClick={this.editar}>
                <FaPencilAlt/>
            </button>
        );
    }

}

export default BotonEditar;