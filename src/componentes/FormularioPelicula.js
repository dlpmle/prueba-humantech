import React from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

class FormularioPelicula extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nombre: this.props.nombre,
            fpublicacion: this.props.fpublicacion,
            imagen: this.props.imagen,
            redireccionar: false
        }
    }

    changeNombreHandler = event => {
        this.setState({
            nombre: event.target.value
        });
    }

    changeFpublicacionHandler = event => {
        this.setState({
            fpublicacion: event.target.value
        });
    }

    changeImagenHandler = event => {
        this.setState({
            imagen: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        axios.post(this.props.url, this.state)
            .then(response => {
                this.setState({
                    nombre: '',
                    fpublicacion: '',
                    imagen: '',
                    redireccionar: true
                });
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                {this.validarRedireccion()}
                <form onSubmit={this.handleSubmit}>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombres</label>
                        <input type="text" className="form-control" id="nombre" name="nombre" placeholder="Ejemplo: Titanic" value={this.state.nombre} onChange={this.changeNombreHandler} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="fpublicacion" className="form-label">Nombres</label>
                        <input type="date" className="form-control" id="fpublicacion" name="fpublicacion" placeholder="Ejemplo: 25/12/2020" value={this.state.fpublicacion} onChange={this.changeFpublicacionHandler} />
                    </div>
                    <div className="input-group mb-3">
                        <input type="file" className="form-control" id="imagen" name="imagen" value={this.state.imagen} onChange={this.changeImagenHandler} />
                        <label className="input-group-text" htmlFor="imagen">Upload</label>
                    </div>
                    <button type="submit">
                        Guardar
                </button>
                </form>
            </div>
        );
    }


    validarRedireccion() {
        //return <Redirect to="/peliculas"/>
        return this.redireccionar ? <Redirect to="/peliculas" /> : <div></div>;
    }
}

export default FormularioPelicula;