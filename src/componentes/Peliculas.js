import React from 'react';
import Navbar from './Navbar';
import SideBar from './SideBar';
import Main from './Main';

class Peliculas extends React.Component {
    render() {
        return (
            <div>
                <Navbar />
                <div className="container-fluid">
                    <div className="row">
                        <SideBar />
                        <Main />
                    </div>
                </div>
            </div>
        );
    }
}

export default Peliculas;