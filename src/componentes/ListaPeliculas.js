import React from 'react';
import Pelicula from './Pelicula';

class ListaPeliculas extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            error: null,
            data: [{
                id: 1,
                nombre: "Titanic",
                fpublicacion: "2001-02-22",
                estado: "Activo"
            }, {
                id: 2,
                nombre: "DBZ-Brolli",
                fpublicacion: "2018-10-11",
                estado: "Activo"
            }]
        }
    }

    render() {
        return (<div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">F.Publicación</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.data.map((item, key) => (this.renderPelicula(item, key)))}
                </tbody>
            </table>
        </div>);
    }

    renderPelicula(i, k) {
        return <Pelicula id={i.id} key={k} nombre={i.nombre} fpublicacion={i.fecha_publicacion} estado={i.estado} />
    }

    componentDidMount() {
                fetch('http://prueba-humantech.test/api/pelicula')
                    .then(res => res.json())
                    .then(
                        (result) => {
                            this.setState({
                                isLoaded: true,
                                data: result
                            });
                        },
                        (error) => {
                            this.setState({
                                isLoaded: true,
                                error
                            });
                        }
                    );
    }
}

export default ListaPeliculas;