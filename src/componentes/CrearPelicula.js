import React from 'react';
import FormularioPelicula from './FormularioPelicula';
import Navbar from './Navbar';
import SideBar from './SideBar';

class CrearPelicula extends React.Component {

    render() {
        return (
            <div>
                <Navbar />
                <div className="container-fluid">
                    <div className="row">
                        <SideBar />
                        <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                            <div className="chartjs-size-monitor">
                                <div className="chartjs-size-monitor-expand">
                                    <div className=""></div>
                                </div>
                                <div className="chartjs-size-monitor-shrink">
                                    <div className=""></div>
                                </div>
                            </div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h1>Películas</h1>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <FormularioPelicula url='http://prueba-humantech.test/api/pelicula'/>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        );
    }

}

export default CrearPelicula;