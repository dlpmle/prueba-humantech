import React from 'react';
import axios from 'axios';
import { useRouteMatch } from 'react-router-dom';
import SideBar from './SideBar';
import Navbar from './Navbar';
import FormularioPelicula from './FormularioPelicula';
import reportWebVitals from '../reportWebVitals';

class ActualizarPelicula extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            fpublicacion: '',
            imagen: '',
            redireccionar: true,
            isLoaded: false
        }
        console.log("en el constuctor");
    }

    getPelicula = async () => {
        let peliculaId = 1;
        try {
            let response = await axios.get(`http://prueba-humantech.test/api/pelicula/${peliculaId}`);
            this.setState({
                nombre: response.data.nombre,
                fpublicacion: response.data.fecha_publicacion,
                imagen: response.data.imagen,
                redireccionar: true
            });
            console.log("Response API");
        } catch (error) {
            console.log(error);
        }

        //let { peliculaId } = useParams();
        //let { peliculaId } = useRouteMatch();
        //console.log(peliculaId);
/*         let peliculaId = 1;
        axios.get(`http://prueba-humantech.test/api/pelicula/${peliculaId}`)
            .then(response => {
                this.setState({
                    nombre: response.data.nombre,
                    fpublicacion: response.data.fecha_publicacion,
                    imagen: response.data.imagen,
                    redireccionar: true
                });
                console.log(response.data.nombre);
            })
            .catch(error => {
                console.log(error);
            }); */

    }
    
    componentDidMount() {
        //this.getPelicula();
        let peliculaId = 1;
        fetch(`http://prueba-humantech.test/api/pelicula/${peliculaId}`)
        .then(res => res.json())
        .then(
            (result) => {
                console.log("Respuesta del API");
                console.log(result.fecha_publicacion);
                this.setState({
                    nombre: result.nombre,
                    fpublicacion: result.fecha_publicacion,
                    imagen: result.ruta_imagen,
                    redireccionar: true
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        );
        this.setState({
            nombre: 'Luis',
            fpublicacion: '2020-01-22',
            imagen: 'www',
            redireccionar: true
        });
    }

    render() {
        console.log("Iteración");
        return (
            <div>
                <Navbar/>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                            <div className="chartjs-size-monitor">
                                <div className="chartjs-size-monitor-expand">
                                    <div className=""></div>
                                </div>
                                <div className="chartjs-size-monitor-shrink">
                                    <div className=""></div>
                                </div>
                            </div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h1>Películas</h1>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <FormularioPelicula url='http://prueba-humantech.test/api/pelicula' nombre={this.state.nombre} fpublicacion={this.state.fpublicacion} imagen={this.state.imagen}/>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActualizarPelicula;